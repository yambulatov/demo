function changeState() {
  if (this.dataset.selected == 'true') {
    this.dataset.selected = 'false';
    this.classList.remove('pack_state_preselected');
    this.classList.remove('pack_state_selected'); //Ie
    this.onmousleave = null;
    this.onblur = null;
  } else {
    this.dataset.selected = 'true';
    this.classList.add('pack_state_preselected');

    this.onblur = function() {
      if (this.dataset.selected == 'true') this.classList.add('pack_state_selected');
      this.onmouseleave = null;
    }
    this.onmouseleave = function() {
      if (this.dataset.selected == 'true') this.classList.add('pack_state_selected');
      this.onmouseleave = null;
    };
  }
}

var packs = document.querySelectorAll('.pack');
for (var i = 0; i < packs.length; i++) {
  if (packs[i].hasAttribute('data-disabled')) {
    packs[i].classList.add('pack_state_disabled');
    packs[i].tabIndex = -1;
  } else {
    packs[i].addEventListener('click', function(e) {
      if (!e.target.classList.contains('pack__footnote')) changeState.call(this);
    })
    packs[i].addEventListener('keydown', function(e) {
      if (e.keyCode === 13 || e.keyCode === 32 ) {
        e.preventDefault();
        changeState.call(this);
      }
    })
  }
}